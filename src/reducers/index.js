import { combineReducers } from 'redux'
import userData from './user'

const reducers = {
    userData
}

const rootReducer = combineReducers(reducers)

export default rootReducer