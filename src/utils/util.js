const serialize = data => {
    const list = [];
    Object.keys(data).forEach(ele => {
      list.push(`${ele}=${data[ele]}`)
    })
    return list.join('&');
  }

  export default {
    serialize
  }