import './App.css';
import React from "react";
import {useRoutes} from "react-router-dom";
import routes from './router'

const App = () => {
  const element = useRoutes(routes)

  // if (loading) return null
  return <>{element}</>
}

export default App;
