import React, { Component } from "react";
import {Button,Input } from 'antd';
import { useNavigate,useHistory  } from 'react-router-dom';
import { UserOutlined,UnlockOutlined  } from '@ant-design/icons';
import './login.scss'
import { login,dict,tenantIdByMobile } from "../../axios/login";
class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            username:'13916833748',
            password:'1'
        }
    }
    
    
   toHomepage=()=>{
        // const histroy=useHistory();
        console.log('点击跳转')
        console.log('用户名:',this.state.username)
        console.log('密码：',this.state.password)
        // navigate('/crleader')
        const data={
            mobile:this.state.username,
            smscode:this.state.password
        }
        console.log('请求参数：',data)
        login(data).then(res=>{
            console.log('react第一请求登录返回值:',res)
        })

        // dict({code:'hot_city'}).then(res=>{
        //     console.log('热门城市:',res)
        // })

        // tenantIdByMobile({mobile:'13916833748'}).then(res=>{
        //     console.log(res)
        // })
    }

    changeUsername=(e)=>{
        this.setState({username:e.target.value})
    }
    changePassword=(e)=>{
        this.setState({password:e.target.value})
    }


    render(){   
        return(
            <section className="login">
                <img src={require('../../assets/images/login-banner.png')} alt="logo" />
                <div className="box">
                    <div className="box-top">
                        <img src={require('../../assets/images/favicon.png')} alt="logo" />
                        香聘顾问
                    </div>
                    <div className="box-note">登陆香聘顾问-我要接单</div>

                    <div className="box-form">
                        <div><Input className="input" size="large" placeholder="请输入手机号" prefix={<UserOutlined/>} onChange={this.changeUsername} value={this.state.username}></Input></div>
                        <div><Input className="input" size="large" placeholder="请输入验证码" prefix={<UnlockOutlined />} onChange={this.changePassword} value={this.state.password}></Input></div>
                        <Button className="btn" type="primary"  danger onClick={this.toHomepage}>登陆</Button>
                    </div>
                
                </div>
            </section>
        )
    }

}
    


export default Login;