import React, { Component } from "react";
import { Link } from 'react-router-dom';
import './header.scss'

class Header extends Component{
    // logout=()=>{
    //     console.log('用户点击退出登录!')
    // }

    login = () =>{
        console.log('用户点击了登录')
    }

    toWorkSpace=()=>{
        console.log('点击了工作台')
    }

    render(){
        return(
            <header className="header">
                <div className="left">
                    <img src={require('../../assets/images/favicon.png')} alt="logo" />
                    香聘顾问
                    <span>全国</span>
                </div>
                <nav className="right">
                    <span><Link to='/'>首页</Link></span>
                    <span>公共人才库</span>
                    <span>沟通</span>
                    <span onClick={this.toWorkSpace}>工作台</span>
                    <span>简历库</span>
                    <span><Link to="/login">登录</Link></span>
                </nav>
            </header>
        );
    }
}

export default Header;