import React, { Component } from "react";
import Header from "../header/Header";
import Footer from "../footer/Footer";
import {Layout} from 'antd';
import HomePage from '../homepage'
import CrLeader from "../crleader";

import { Routes,Route } from "react-router";
const {
    Content
  } = Layout;

class Main extends Component{




    render(){
        return(
            <Layout>
                <Header/>
                <Routes>
                    <Route exact path='/' element={<HomePage/>} />
                    <Route path='/homepage' element={<HomePage/>} />
                    <Route path='/crleader' element={<CrLeader/>} />
                </Routes>
                <Footer/>
           </Layout>
        )
    }
}

export default Main;