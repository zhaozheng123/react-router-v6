import createHistory from './history';
import {createStore,combineReducers} from 'redux';
import {syncHistoryWithStore,routerReducer} from 'react-router-redux';
import rootReduce from '../reducers/index';
import createBrowserHistory from './history'




const store=createStore(combineReducers({
    ...rootReduce,
    routing: routerReducer
  }))
export const history=syncHistoryWithStore(createBrowserHistory,store)

export default store;