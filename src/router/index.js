import React from "react";
import loadable from '@loadable/component';
const Main = loadable(() => import('../pages/index/index'));
const Login = loadable(() => import('../pages/login/Login'));
const CrLeader = loadable(() => import('../pages/crleader'));
const HomePage = loadable(() => import('../pages/homepage'));
const routes=[{
    path:"/",
    name:'first-page',
    key: '/',
    element:<Main/>,
    children:[
        {
            path:'homepage',
            name:'home-page',
            key: '/homepage',
            element:<HomePage/>,
            // onEnter:onRouteEnter,
        },
        {
            path:'crleader',
            name:'crleader-page',
            key: '/crleader',
            element:<CrLeader/>,
            // onEnter:onRouteEnter,
        }
    ]
},{
    path:"login",
    name:'login-page',
    key: '/login',
    element:<Login/>,
    // onEnter:onRouteEnter,
    // meta:{
    //     icon:'desktop',
    //     title:'登录'
    // }
}]


function onRouteEnter(nextState, replace, callback) {
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
    // const route = nextState.routes[nextState.routes.length - 1].path ? nextState.routes[nextState.routes.length - 1] : nextState.routes[0]
    // const storeData = store.getState().userData
    // const {access, hasGetInfo} = storeData
    // let token = getToken()
    // if (!token && route.path === '/login') {
    // } else if (!token && route.path !== '/login') {
    //     replace({
    //         pathname: "/login"
    //     })
    // } else if (token && route.path === '/login') {
    //     replace({
    //         pathname: "/"
    //     })
    // } else {
    //     if (hasGetInfo) {
    //         canChangeRouter(route, access, callback, replace)
    //     }
    // }
    // addRouterHistory(route)
    // callback()
}

export default routes