import request from './index';
// import axios from 'axios'

export const login = function (data) {
    return request({
        url: '/renrui-auth/oauth/token',
        headers: {
          'Tenant-Id': '423638',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic c2FiZXI6c2FiZXJfc2VjcmV0'
        },
        method: 'post',
        params: {
          ...data,
          grant_type: 'sms_code',
          code:'zhizhao_common_code'
        }
    })
}

//获取热门城市
export const dict = function (params) {
  return request({
    url: '/renrui-user-admin/dictionary/listByCode',
    method: 'get',
    params
  })
}


export const tenantIdByMobile = function (params) {
  return request({
    url: '/renrui-supplier/renruisuppliercompany/listByMobile',
    method: 'get',
    params
  })
}
