import axios from 'axios';

import { message } from 'antd';


// 允许携带 cookie
axios.defaults.withCredentials = false;

const instance = axios.create({
    baseURL: 'https://ruitest.renruihr.com/api',
    headers:{'Content-Type':'application/x-www-form-urlencoded'},
    timeout: 10000
});

instance.interceptors.request.use(config => {
  // config.headers['Authorization'] = 'Basic c2FiZXI6c2FiZXJfc2VjcmV0';
  // config.headers['User-Platform'] = 'zhizhao';
  // config.headers['Channel'] = 4;
  // //让每个请求携带token
  // if (getToken() && !isToken) {
  //   config.headers['Blade-Auth'] = config.headers['Blade-Auth'] || 'bearer ' + getToken()
  // }
  // //headers中配置text请求
  // if (config.text === true) {
  //   config.headers["Content-Type"] = config.headers["Content-Type"] || "text/plain";
  // }
  // //headers中配置serialize为true开启序列化
  // if (config.method === 'post' && meta.isSerialize === true) {
  //   config.data = serialize(config.data);
  // }
  return config;
}, err => {
    message.open({
        content: "网络连接失败",
        duration: 2
    });
});


instance.interceptors.response.use(res => {
    return res.data;
}, err =>{
    let content;
    if (err || err.response) {
        let status = err.response?.status ?? 0;
        switch (status) {
            case 400: content = "请求错误";
                break;

            case 401: content = "未授权访问";
                break;

            case 500: content = "服务器崩溃~";
                break;

            default: content = "服务器无响应";
        }
    }
    message.open({ content, duration: 2 });
    return err;
});

export default instance;
